//----------------------------------------------------------------------------------------------------------------------
// Main server module for s2-server
//
// @module server.js
//----------------------------------------------------------------------------------------------------------------------

var express = require('express');
var bodyParser = require('body-parser');

// Config
var config = require('./config');

// Routes
var s2Router = require('./server/routes/s2');

//----------------------------------------------------------------------------------------------------------------------

// Build the express app
var app = express();

// JSON Body support
app.use(bodyParser.json());

// Set up our application routes
app.use('/s2', s2Router);

// Start the server
var server = app.listen(config.http.port || 3000, function()
{
    var host = server.address().address;
    var port = server.address().port;

    console.log('PokemonGoCompanion v%s listening at http://%s:%s', require('./package').version, host, port);
});

//----------------------------------------------------------------------------------------------------------------------

