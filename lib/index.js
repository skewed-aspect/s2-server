//---------------------------------------------------------------------------------------------------------------------
// Get the S2 cells covering an area
//---------------------------------------------------------------------------------------------------------------------

var fs = require('fs');
var _ = require('lodash');
var s2 = require('s2');

//---------------------------------------------------------------------------------------------------------------------

function query()
{
    //var points = [
    //	new s2.S2LatLng(33.52393745151943, -101.9527816772461);
    //	new s2.S2LatLng(33.62319623534052, -101.9527816772461),
    //	new s2.S2LatLng(33.52393745151943, -101.9527816772461)
    //];

    // Covering lubbock
    var ne = new s2.S2LatLng(33.63577420745804,-101.75296783447266);
    var sw = new s2.S2LatLng(33.496456660211564,-102.03071594238281);

    var rect = new s2.S2LatLngRect(ne, sw);

    var cover = s2.getCoverSync(rect, {
        min: 15,
        max: 15
    });

    var json = {
        type: "FeatureCollection",
        features: _(cover).map(function(cell)
            {
                var cellObj = cell.toGeoJSON();
                cellObj.properties = {
                    id: cell.id().id(),
                    centerPoint: cell.id().toLatLng().toGeoJSON()
                };

                return cellObj;
            })
            .compact()
            .value()
    };

    console.log('writing out file!');
    fs.writeFileSync('./lib/cells.json', JSON.stringify(json, null, 4));
    console.log('Done!');
} // end query


//---------------------------------------------------------------------------------------------------------------------

module.exports = {
    init: query
};

//---------------------------------------------------------------------------------------------------------------------
