FROM node:0.10

EXPOSE 8080

MAINTAINER Christopher S. Case <chris.case@g33xnexus.com>

RUN mkdir -p /app
WORKDIR /app

ADD . /app/

VOLUME /app/server

RUN npm install

CMD [ "node", "server.js" ]
