//----------------------------------------------------------------------------------------------------------------------
// Routes for the list of all 151 pokemon
//
// @module
//----------------------------------------------------------------------------------------------------------------------

var _ = require('lodash');
var s2 = require('s2');
var express = require('express');

//----------------------------------------------------------------------------------------------------------------------

var router = express.Router();

//----------------------------------------------------------------------------------------------------------------------
// REST Endpoints
//----------------------------------------------------------------------------------------------------------------------

router.get('/cover/:nePt/:swPt', function(req, resp)
{
    if(req.params.nePt && req.params.swPt)
    {
        // Parse out points (Latitude, Longitude)
        var nePt = _.map(req.params.nePt.split(','), function(part)
        {
            return parseFloat(part);
        });
        var swPt = _.map(req.params.swPt.split(','), function(part)
        {
            return parseFloat(part);
        });

        // S2 Points
        var ne = new s2.S2LatLng(nePt[0], nePt[1]);
        var sw = new s2.S2LatLng(swPt[0], swPt[1]);

        // S2Rect
        var rect = new s2.S2LatLngRect(ne, sw);


        // Generate cover
        var cover;
        try
        {
            //FIXME: Using anything other than 'Sync' seems to not respect the options we pass in...
            cover = s2.getCoverSync(rect, {
                min: 15,
                max: 15
            });
        }
        catch(err)
        {
            resp.status(500).json({
                type: error.name,
                message: error.message,
                stack: error.stack.split('\n')
            });
        } // end catch

        var json;
        if(req.query.geojson)
        {
            json = {
                type: "FeatureCollection",
                features: _(cover).map(function(cell)
                {
                    var cellObj = cell.toGeoJSON();
                    cellObj.properties = {
                        id: cell.id().id(),
                        centerPoint: cell.id().toLatLng().toGeoJSON()
                    };

                    return cellObj;
                })
                    .compact()
                    .value()
            };
        }
        else
        {
            json = _(cover).map(function(cell)
            {
                return cell.id().id();
            })
                .compact()
                .value()
        } // end if

        resp.json(json);
    }
    else
    {
        resp.status(400).json({
            type: 'InvalidError',
            message: "Your request was invalid. You shouldd feel bad."
        });
    } // end if
});

//----------------------------------------------------------------------------------------------------------------------

module.exports = router;

//----------------------------------------------------------------------------------------------------------------------
