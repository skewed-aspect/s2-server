# S2 Server

This is a very simple express application that serves up requests to the 
[s2-node][s2-node] library. The whole reason I wrote this is because `s2-node`
doesn't run on node.js v0.12 and newer. So, I wrote a web server, and run it in
a docker, which works great for what I need.

## Endoints

### `/s2/cover/<nePt>/<swPt>/`
 * Params:
   * `nePt` - The 'north east' point of the bounding box, in `Latitude,Longitude` format.
   * `swPt` - The 'south west' point of the bounding box, in `Latitude,Longitude` format.
 * Query:
   * `geojson` - Set to `geojson=true` to get the covering cells back in geojson format.
   
Gets the cells that cover the bounding box specified by a north east point, 
and a south west point.